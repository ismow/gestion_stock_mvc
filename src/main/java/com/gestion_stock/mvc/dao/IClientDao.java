package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.Client;

public interface IClientDao extends IGenericDao<Client> {

}
