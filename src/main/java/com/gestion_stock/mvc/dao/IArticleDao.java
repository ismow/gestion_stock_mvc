package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.Article;

public interface IArticleDao extends IGenericDao<Article> {

}
