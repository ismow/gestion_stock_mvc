package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.Categorie;

public interface ICategorieDao extends IGenericDao<Categorie>{

}
