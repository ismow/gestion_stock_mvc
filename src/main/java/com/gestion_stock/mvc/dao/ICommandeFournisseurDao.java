package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.CommandeFournisseur;

public interface ICommandeFournisseurDao extends IGenericDao<CommandeFournisseur> {

}
