package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.Vente;

public interface IVenteDao extends IGenericDao<Vente> {

}
