package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.LigneCommandeClient;

public interface ILigneCommandeClientDao extends IGenericDao<LigneCommandeClient> {

}
