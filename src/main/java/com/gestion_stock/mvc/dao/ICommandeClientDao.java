package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.CommandeClient;

public interface ICommandeClientDao extends IGenericDao<CommandeClient>{

}
