package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.Fournisseur;

public interface IFournisseurDao extends IGenericDao<Fournisseur> {

}
