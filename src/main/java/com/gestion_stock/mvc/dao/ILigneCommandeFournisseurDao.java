package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
