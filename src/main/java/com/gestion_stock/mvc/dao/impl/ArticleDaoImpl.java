package com.gestion_stock.mvc.dao.impl;

import com.gestion_stock.mvc.dao.IArticleDao;
import com.gestion_stock.mvc.entities.Article;

public class ArticleDaoImpl extends GenericDaoImpl<Article> implements IArticleDao {

}
