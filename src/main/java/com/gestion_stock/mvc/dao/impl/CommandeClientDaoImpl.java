package com.gestion_stock.mvc.dao.impl;

import com.gestion_stock.mvc.dao.ICommandeClientDao;
import com.gestion_stock.mvc.entities.CommandeClient;

public class CommandeClientDaoImpl extends GenericDaoImpl<CommandeClient> implements ICommandeClientDao {

}
