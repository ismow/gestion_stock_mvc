package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.LigneVente;

public interface ILigneVenteDao extends IGenericDao<LigneVente> {

}
