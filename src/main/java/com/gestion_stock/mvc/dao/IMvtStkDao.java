package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.MvtStk;

public interface IMvtStkDao extends IGenericDao<MvtStk> {

}
