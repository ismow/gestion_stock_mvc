package com.gestion_stock.mvc.dao;

import com.gestion_stock.mvc.entities.Utilisateur;

public interface IUtilisateurDao extends IGenericDao<Utilisateur> {

}
